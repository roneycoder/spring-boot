package com.example.spring.curd.controller;


import com.example.spring.curd.Service.BookService;
import com.example.spring.curd.model.bookModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class BookController {
    @Autowired
    BookService service;

    @GetMapping("GetAll")
    public List<bookModel> getAllBooks() {
        System.out.println(service.getAllBooks());
        System.out.println("details");
        return service.getAllBooks();
    }

    @PostMapping("addBooks")
    public void addBooks(@RequestBody bookModel book) {
        System.out.println("passing to service");
        service.addBooks(book);
    }

    @GetMapping("get/{id}")
    public Optional<bookModel> gerById(@PathVariable long id) {
        System.out.println("passing to service");
        System.out.println(service.getById(id));
        return service.getById(id);

    }

    @DeleteMapping("deleteById/{id}")
    public void deleteById(@PathVariable long id) {
        service.deleteById(id);
    }
}
