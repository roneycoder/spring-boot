package com.example.spring.curd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.spring.curd.model.country;
import com.example.spring.curd.service.filteredCountry;

@RestController
public class filterCountryController {
	@Autowired
	filteredCountry country;

	@GetMapping("filter")

	public List<country> getCountry() {

		return country.getFilteredList();

	}

	@GetMapping("updateCountry")
	public void updateCountry() {

		country.updateCountry();
	}

	@GetMapping("dupicate")

	public void removeDuplicate() {

		country.removeDuplicate();
	}

}
