package com.example.spring.curd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.spring.curd.model.country;
import com.example.spring.curd.service.countryService;

@RestController
public class countryController {
	@Autowired
	countryService country;

	@GetMapping("/get")
	public List<country> getcountryList() {

		return country.takeCountry();

	}

	@PostMapping("add")
	public String addCountry(@RequestBody country c) {
		return country.addCountry(c);

	}

	@GetMapping("getCountry/{id}")
	public country getCountry(@PathVariable("id") int id) throws Exception {

		return country.getCountry(id);
	}

	@DeleteMapping("delete/{id}")
	public String delete(@PathVariable("id") int id) {

		return country.deleteCountry(id);

	}

	@GetMapping("getsame")
	public List<country> getSameList() {

		return country.getSameList();
	}

	@GetMapping("getdesc")
	public List<country> getDescOrder() {
		return country.getCountryDesc();

	}

	@GetMapping("getnumericcode")
	public List<country> getNumericCode() {
		return country.getOnNumricCode();

	}

}
