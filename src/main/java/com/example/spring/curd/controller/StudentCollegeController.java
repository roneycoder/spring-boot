package com.example.spring.curd.controller;

import com.example.spring.curd.Service.collegeStudentService;
import com.example.spring.curd.model.collegeStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentCollegeController {
    @Autowired
    collegeStudentService service;


    @GetMapping("addStudentDetails")
    public void addStudent(collegeStudent student) {
        student.setAddress("parbhai");
        student.setCity("parbhai");
        student.setSname("khan");
        service.addStudentDetails(student);
    }

    @GetMapping("getAllStudent")
    public List<collegeStudent> getAllDetails() {
        System.out.println("incontrller");
        System.out.println(service.getDetails());
        return service.getDetails();
    }

}
