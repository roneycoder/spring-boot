package com.example.spring.curd.controller;

import com.example.spring.curd.Service.StudentService;
import com.example.spring.curd.model.StudentData;
import com.example.spring.curd.repo.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class StudentController {
    @Autowired
    StudentService service;

    StudentRepo repo;

    @RequestMapping("/")
    public void test() {
        System.out.println("test2");
    }

//    @RequestMapping(value = "getStudentDetails", method = RequestMethod.GET)
//    public List<StudentData> getAllStudentDetails() {
//        System.out.println(service.getAllDetails());
//        return service.getAllDetails();
//
//    }

    @PostMapping("addStudentDetails")
    public void add(@RequestBody StudentData data) {
        service.add(data);
        System.out.println("passed to service");

    }
//
//    @GetMapping("/{id}")
//    public StudentData findById(@PathVariable long id) {
//
//        return repo.findById(id);
//    }


    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        repo.deleteById(id);
        System.out.println("Deleted");
    }
}
