package com.example.spring.curd.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.spring.curd.model.country;
import com.example.spring.curd.model.api.countryApi;
import com.example.spring.curd.repo.countryRepo;

@Service
public class countryApiService {
	@Autowired
	countryRepo repo;

	public void addCountry() {

		RestTemplate template = new RestTemplate();
		ResponseEntity<List<countryApi>> response = template.exchange("https://restcountries.eu/rest/v2/all",
				HttpMethod.GET, null, new ParameterizedTypeReference<List<countryApi>>() {
				});
		List<countryApi> country = response.getBody();
		System.out.println(country);

		for (countryApi api : country) {
			country c = new country();
			c.setName(api.getName());
			c.setNativeName(api.getNativeName());
			c.setNumericCode(api.getNumericCode());
			c.setAlpha2Code(api.getAlpha2Code());
			repo.save(c);

		}
	}

}
