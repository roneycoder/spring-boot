package com.example.spring.curd.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.curd.model.country;
import com.example.spring.curd.repo.countryRepo;

@Service
public class filteredCountry {

	@Autowired
	countryRepo repo;

	public List<country> getFilteredList() {

		List<country> countryList = repo.findAll();
		long filteredList = countryList.stream().count();

		// List<country> fList =
		// countryList.stream().sorted().collect(Collectors.toList());
		List<country> fList = countryList.stream().filter(i -> i.getName().startsWith("Z"))
				.collect(Collectors.toList());
		System.out.println(fList);

		return fList;

	}

	public void updateCountry() {

		List<country> updateList = repo.findAll();
		for (country c : updateList) {
			country co = new country();
			if (c.getAlpha2Code() == null || c.getName() == null || c.getNativeName() == null
					|| c.getNumericCode() == null) {
				System.out.println(c.getAlpha2Code() + "=========================");
				repo.delete(c);
			}

			if (c.getNumericCode().equals("248")) {
				c.setName("wasik");
				System.out.println("================");
				repo.save(c);
			}
		}
	}

	public void removeDuplicate() {
		// TODO Auto-generated method stub

		Set<country> set = new HashSet();

		List<country> updateList = repo.findAll();
		for (country c : updateList) {
			country co = new country();
			// System.out.println(c);
			set.add(c);
			System.out.println(set);

		}

	}
}
