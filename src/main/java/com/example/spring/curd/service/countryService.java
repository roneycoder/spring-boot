package com.example.spring.curd.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.spring.curd.model.country;
import com.example.spring.curd.repo.countryRepo;

@Service
public class countryService {

	@Autowired
	countryRepo repo;

	public List<country> takeCountry() {
		country c = new country();

		return repo.findAll();

	}

	public String addCountry(country c) {
		String error = "failed";
		if (c != null) {
			repo.save(c);
			error = "success";
		}
		return error;

	}

	public country getCountry(int id) throws Exception {
		Optional<country> c = repo.findById(id);
		if (c.isPresent()) {
			System.out.println("=======================" + c.isPresent());

			System.out.println("==================object=====" + c);
			return c.get();
		} else {
			throw new Exception("No country record exist for given id");
		}

	}

	public String deleteCountry(int id) {
		String error = "failed";
		Optional<country> co = repo.findById(id);
		if (co.isPresent()) {
			repo.deleteById(id);
			error = "success";
		}

		return error;

	}

	public List<country> getSameList() {
		List<country> list = repo.findAll();

		List<country> sameLsit = list.stream().filter(i -> i.getName().equals("wasik")).collect(Collectors.toList());

		return sameLsit;

	}

	public List<country> getCountryDesc() {
		List<country> list = repo.findAll();

		List<country> descname = list.stream().sorted((s1, s2) -> s2.getAlpha2Code().compareTo(s1.getAlpha2Code()))
				.collect(Collectors.toList());

		return descname;
	}

	public List<country> getOnNumricCode() {

		List<country> list = repo.findAll();
		List<country> numericCode = list.stream().sorted((s1, s2) -> s2.getNumericCode().compareTo(s1.getNumericCode()))
				.collect(Collectors.toList());

		return numericCode;

	}

}
