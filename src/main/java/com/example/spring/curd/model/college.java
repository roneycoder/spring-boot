package com.example.spring.curd.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class college {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String collagename;
    @Column
    private String city;


}
