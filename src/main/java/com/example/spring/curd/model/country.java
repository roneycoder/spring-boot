package com.example.spring.curd.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class country {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int countryId;
	public String alpha2Code;
	public String numericCode;
	public String nativeName;
	public String name;

	public country(String alpha2Code, String numericCode, String nativeName, String name) {
		super();
		this.alpha2Code = alpha2Code;
		this.numericCode = numericCode;
		this.nativeName = nativeName;
		this.name = name;
	}

	@Override
	public String toString() {
		return "countryApi [alpha2Code=" + alpha2Code + ", numericCode=" + numericCode + ", nativeName=" + nativeName
				+ ", name=" + name + "]";
	}

	public country() {
		super();
	}

	public String getAlpha2Code() {
		return alpha2Code;
	}

	public void setAlpha2Code(String alpha2Code) {
		this.alpha2Code = alpha2Code;
	}

	public String getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}

	public String getNativeName() {
		return nativeName;
	}

	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
