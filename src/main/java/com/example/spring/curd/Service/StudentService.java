package com.example.spring.curd.Service;

import com.example.spring.curd.model.StudentData;
import com.example.spring.curd.repo.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentRepo repo;

//    public List<StudentData> getAllDetails() {
//        System.out.println("inside service");
//        return repo.findAll();
//
//    }

    public void add(StudentData data) {
        repo.save(data);
        System.out.println("added");
    }


}
