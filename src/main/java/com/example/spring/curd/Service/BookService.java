package com.example.spring.curd.Service;

import com.example.spring.curd.model.bookModel;
import com.example.spring.curd.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    @Autowired
    BookRepo repo;

    public List<bookModel> getAllBooks() {

        return (List<bookModel>) repo.findAll();
    }

    public void addBooks(bookModel book) {
        repo.save(book);
    }

    public Optional<bookModel> getById(long id) {
        Optional<bookModel> fooOptional = repo.findById(id);
        return fooOptional;
    }

    public void deleteById(long id) {
        repo.deleteById(id);
    }

}
