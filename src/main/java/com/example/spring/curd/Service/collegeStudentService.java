package com.example.spring.curd.Service;

import com.example.spring.curd.model.collegeStudent;
import com.example.spring.curd.repo.CollegeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class collegeStudentService {
    @Autowired
    CollegeRepo repo;

    public void addStudentDetails(collegeStudent clg) {
        repo.save(clg);

    }

    public List<collegeStudent> getDetails() {
        return (List<collegeStudent>) repo.findAll();
    }
}

