package com.example.spring.curd.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.spring.curd.model.country;

@Repository
public interface countryRepo extends JpaRepository<country, Integer> {

}
