package com.example.spring.curd.repo;

import com.example.spring.curd.model.collegeStudent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CollegeRepo extends CrudRepository<collegeStudent, Integer> {
}
