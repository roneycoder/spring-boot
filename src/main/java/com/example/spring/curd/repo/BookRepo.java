package com.example.spring.curd.repo;

import com.example.spring.curd.model.bookModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo extends JpaRepository<bookModel, Long> {

}
