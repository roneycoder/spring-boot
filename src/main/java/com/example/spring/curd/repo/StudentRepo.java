package com.example.spring.curd.repo;

import com.example.spring.curd.model.StudentData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepo extends JpaRepository<StudentData, Long> {
}
